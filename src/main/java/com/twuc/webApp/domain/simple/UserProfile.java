package com.twuc.webApp.domain.simple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;

    private Short yearOfBirth;

    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    private UserProfile(String name, Short yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public Short getYearOfBirth() {
        return this.yearOfBirth;
    }

    public UserProfile() {
    }

    public void setName(String name) {
        this.name = name;
    }
}
